#!/usr/bin/env bashio

CONFIG_JSON=$(bashio::config 'config_json')
DEBUG=$(bashio::config 'debug')
export CONFIG_PATH=/app/

echo "${CONFIG_JSON}" > "${CONFIG_PATH}config.json"
chmod 0600 "${CONFIG_PATH}config.json"

if [ "${DEBUG}"  = "true" ]; then
    cat "${CONFIG_PATH}config.json"

    echo "IPs:"
    ip addr || true
    echo "IPv4:"
    curl -v 'https://1.1.1.1/cdn-cgi/trace' || true
    echo "IPv6:"
    curl -v 'https://[2606:4700:4700::1111]/cdn-cgi/trace' || true
    echo "Supervisor Network Config:"
    curl -sSL -H "Authorization: Bearer $SUPERVISOR_TOKEN" http://supervisor/network/info || true
    echo
fi

cd "${CONFIG_PATH}" || exit 1

echo "Running cloudflare-ddns ..."
/app/cloudflare-ddns --repeat
