# Home Assistant Add-on: Cloudflare DDNS

Cloudflare offers free CDN including DNS Hosting.

## About

With this Add-on you can use your own Domain for Dynamic DNS using Cloudflares DNS Service.

## Installation

Follow these steps to get the add-on installed on your system:

1. Navigate in your Home Assistant frontend to **Supervisor -> Add-on Store.**
1. Find the "letsencrypt" add-on and click it.
1. Click on the "INSTALL" button.

## How to use

To use this addon, you can choose between Cloudflare API Token (recommened) and API Key.

#### Option `config_json`

This is the Cloudflare ddns `config.json`, see https://github.com/timothymiller/cloudflare-ddns/blob/master/config-example.json.

The API token Token permissions are required:

- Permissions
  - Zone:Zone:Read
  - Zone:DNS:Edit
- Zone Resources
  - All or just a specific Zone(s)

Unfortunately, it's currently not possible to give the token only permissions on the desired Zone, this is a known
issue at Cloudflare, hopefully this will be fixed in future.

Example `config.json`:

```json
{
    "cloudflare": [
        {
            "authentication": {
                "api_token": "API_TOKEN"
            },
            "zone_id": "ZONE_ID",
            "subdomains": ["hassio"],
            "proxied": false
        }
    ],
    "a": false,
    "aaaa": true,
    "purgeUnknownRecords": false
}
```

#### Option `retry_interval`

The time in *seconds* were the Addon will check for IP changes.

#### Option `log_level`

Sets the desired log level. Following options are available:

- fatal *(default)*
- error
- warn
- info
- debug
