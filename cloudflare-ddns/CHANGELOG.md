# Changelog

## 0.5.9

- Enable hassio_api in addon config

## 0.5.8

- Fix CONFIG_PATH for good

## 0.5.7

- update galexrt/cloudflare-ddns

## 0.5.6

- Use Hassio Supervisor API for IPs

## 0.5.5

- Add more debug output

## 0.5.4

- add DEBUG variable

## 0.5.3

- Fix CONFIG_PATH

## 0.5.2

- Use cloudflare-ddns --repeat flag

## 0.5.1

- Fix config reading issues

## 0.5.0

- Use https://github.com/timothymiller/cloudflare-ddns
- Rename to cloudflare-ddns

## 0.4.0

- Updated to cloudflare-ddns v1.3.0
- [#1](https://gitlab.com/galexrt/hassio-addons/issues/1) - Implemented support for root domain using '@'.

## 0.3.0

- Updated to cloudflare-ddns v1.2.0
- Added support to set *log_level*

## 0.2.1

- Fixed invalid config.json

## 0.2.0

- Added *retry_interval* for continous update

## 0.1.2

- Fixed run.sh syntax error

## 0.1.1

- Fixed permission denied error

## 0.1.0

- Initial version
